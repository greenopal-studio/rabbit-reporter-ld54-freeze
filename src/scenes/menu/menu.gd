extends Control


func _ready():
	%StartButton.pressed.connect(_on_start_pressed)
	%SettingsButton.pressed.connect(_on_settings_pressed)
	

func _on_start_pressed() -> void:
	SCENE.goto_scene(ID.SCENE_TYPES.WORLD_MAP)


func _on_settings_pressed() -> void:
	SCENE.goto_scene(ID.SCENE_TYPES.SETTING)
