extends Control


const POSITIVE_PRESTIGE_CHANGE_MULTIPLIER = 20
const NEUTRAL_PRESTIGE_CHANGE = 5
const NEGATIVE_PRESTIGE_CHANGE_MULTIPLIER = 10
const ALLOWED_ADS_MAX = 2


@onready var _newspaper := %Newspaper
@onready var _article_score_label := %ArticleScoreLabel
@onready var _empty_space_container := %EmptySpaceGridContainer
@onready var _too_many_ads_container := %TooManyAdsGridContainer
@onready var _ads_count_label := %AdsCountLabel
@onready var _gov_bad_container := %GovBadContainer
@onready var _gov_good_container := %GovGoodContainer
@onready var _ad_synergy_container := %AdSynergyContainer
@onready var _final_score_label := %FinalScoreLabel
@onready var _prestige_change_label := %PrestigeChangeLabel


var _story_scene = preload('res://scenes/newspaper/newspaper_story.tscn')


var _prestige_change: float = 0.0


func _ready():
	%PublishButton.pressed.connect(_on_publish_button_pressed)

	_set_data()


func _set_data():
	var stories_count := 0
	var stories_rating_acc := 0
	var covered_grid_count := 0
	var ad_count := 0
	var has_gov_bad := false
	var has_gov_good := false
	var has_ad_synergy := false

	var _newspaper_grid_x := int(_newspaper.size.x / 3)
	var _newspaper_grid_y := int(_newspaper.size.y / 4)

	for article in STATE.get_placed_stories():
		covered_grid_count += (
			int(article.get_size().x) * int(article.get_size().y)
		)
		if article.get_used_story_id() != -1:
			stories_count += 1
			# accumulate rating based on NPS (vaguely...)
			# 1-3 = negative , 4 = neutral , 5 = positive
			stories_rating_acc += article.get_rating() - 4
			var story_node = _story_scene.instantiate()
			_newspaper.add_child(story_node)
			story_node.width = article.get_size().x * _newspaper_grid_x
			story_node.height = article.get_size().y * _newspaper_grid_y
			story_node.position = Vector2(
				article.get_pos().x * _newspaper_grid_x,
				article.get_pos().y * _newspaper_grid_y
			)
			story_node.is_headline = article.get_is_headline()
			story_node.title = article.get_headline()
			story_node.show_shadow = false
			story_node.rating = article.get_rating()

		elif article.get_used_ad_id() != -1:
			ad_count += 1
			var ad_data = DATA.ads[article.get_used_ad_id()]
			if not has_ad_synergy:
				var synergy_stories_ids = ad_data.get_story_synergy_ids()
				for article2 in STATE.get_placed_stories():
					if article2.get_used_story_id() in synergy_stories_ids:
						has_ad_synergy = true
						break
			var ad_node = _story_scene.instantiate()
			_newspaper.add_child(ad_node)
			ad_node.width = _newspaper_grid_x * 1
			ad_node.height = _newspaper_grid_y * 1
			ad_node.position = Vector2(
				article.get_pos().x * _newspaper_grid_x,
				article.get_pos().y * _newspaper_grid_y
			)
			ad_node.is_ad = true
			ad_node.set_ad_texture(ad_data.get_texture())
			ad_node.show_shadow = false

	var score = 0.0
	if stories_count > 0:
		score = 4.0 + (float(stories_rating_acc) / float(stories_count))
	_article_score_label.text = '%1.2f' % score

	var has_empty_space = covered_grid_count < 12
	if has_empty_space:
		score -= 1.0
	_empty_space_container.visible = has_empty_space

	_ads_count_label.text = '(%d Ads)' % ad_count

	var has_too_many_ads = ad_count > ALLOWED_ADS_MAX
	if has_too_many_ads:
		score -= 1.0
	_too_many_ads_container.visible = has_too_many_ads

	if has_gov_bad:
		score -= 0.5
	_gov_bad_container.visible = has_gov_bad

	if has_gov_good:
		score += 0.5
	_gov_good_container.visible = has_gov_good

	if has_ad_synergy:
		score += 0.5
	_ad_synergy_container.visible = has_ad_synergy

	score = max(0.0, score)

	_final_score_label.text = '%1.2f' % score

	_prestige_change = 0.0
	if score >= 5.0:
		_prestige_change = (score - 4.0) * POSITIVE_PRESTIGE_CHANGE_MULTIPLIER
	elif score >= 4.0:
		_prestige_change = NEUTRAL_PRESTIGE_CHANGE
	else:
		_prestige_change = (score - 4.0) * NEGATIVE_PRESTIGE_CHANGE_MULTIPLIER

	_prestige_change_label.text = '%s%d' % [
		'+ ' if _prestige_change > 0 else '- ',
		abs(_prestige_change)
	]


func _on_publish_button_pressed():
	STATE.update_prestige.emit(_prestige_change)
	STATE.new_day.emit()
	SCENE.goto_scene(ID.SCENE_TYPES.WORLD_MAP)
