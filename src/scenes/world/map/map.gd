extends Node2D


@onready var _rabbit_university = %RabbitUniversity
@onready var _rabbit_university_label = %RabbitUniversity/SubViewport/Label
@onready var _vegetable_patch = %VegetablePatch
@onready var _vegetable_patch_label = %VegetablePatch/SubViewport/Label
@onready var _dining_area = %DiningArea
@onready var _dining_area_label = %DiningArea/SubViewport/Label
@onready var _office = %Office
@onready var _office_label = %Office/SubViewport/Label


var _pointer_mouse = 0 #counts how often the mouse was called to be a pointer 


func _ready():
	var __current_theme = ThemeService.get_current_theme().duplicate(true)
	__current_theme.set_default_font_size(__current_theme.get_default_font_size() * 1.5)
	_rabbit_university_label.set_theme(__current_theme)
	_rabbit_university_label.add_theme_constant_override('outline_size', __current_theme.get_default_font_size() / 2.0)
	_vegetable_patch_label.set_theme(__current_theme)
	_vegetable_patch_label.add_theme_constant_override('outline_size', __current_theme.get_default_font_size() / 2.0)
	_dining_area_label.set_theme(__current_theme)
	_dining_area_label.add_theme_constant_override('outline_size', __current_theme.get_default_font_size() / 2.0)
	_office_label.set_theme(__current_theme)
	_office_label.add_theme_constant_override('outline_size', __current_theme.get_default_font_size() / 2.0)
	
	_rabbit_university.mouse_entered.connect(_highlight_area.bind(_rabbit_university, _rabbit_university_label))
	_rabbit_university.mouse_exited.connect(_unhighlight_area.bind(_rabbit_university, _rabbit_university_label))
	_rabbit_university.input_event.connect(_on_area_click.bind(ID.SCENE_TYPES.RABBIT_UNIVERSITY))
	_vegetable_patch.mouse_entered.connect(_highlight_area.bind(_vegetable_patch, _vegetable_patch_label))
	_vegetable_patch.mouse_exited.connect(_unhighlight_area.bind(_vegetable_patch, _vegetable_patch_label))
	_vegetable_patch.input_event.connect(_on_area_click.bind(ID.SCENE_TYPES.VEGETABLE_PATCH))
	_dining_area.mouse_entered.connect(_highlight_area.bind(_dining_area, _dining_area_label))
	_dining_area.mouse_exited.connect(_unhighlight_area.bind(_dining_area, _dining_area_label))
	_dining_area.input_event.connect(_on_area_click.bind(ID.SCENE_TYPES.DINING_AREA))
	_office.mouse_entered.connect(_highlight_area.bind(_office, _office_label))
	_office.mouse_exited.connect(_unhighlight_area.bind(_office, _office_label))
	_office.input_event.connect(_on_area_click.bind(ID.SCENE_TYPES.ASSEMBLE_NEWSPAPER))


func _highlight_area(__area: Area2D, __label: Label) -> void:
	__area.get_node('Background').show()
	__area.get_node('Border').show()
	__label.show()
	_pointer_mouse += 1


func _unhighlight_area(__area: Area2D, __label: Label) -> void:
	__area.get_node('Background').hide()
	__area.get_node('Border').hide()
	__label.hide()
	_pointer_mouse -= 1


func _on_area_click(__viewport: Node, __event: InputEvent, __shape_idx: int, __scene_id: ID.SCENE_TYPES) -> void:
	if __event is InputEventMouseButton and __event.is_pressed() and __event.button_index == MOUSE_BUTTON_LEFT:
		SCENE.goto_scene(__scene_id)
