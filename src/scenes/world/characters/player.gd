extends CharacterBody2D


const MOVE_SPEED = 420.0
const HOP_SPEED = 200.0
const SPRITE_POS = Vector2(0, -230)

@onready var _sprite = $Sprite
@onready var _detection_area = $Area2D
@onready var _animation_player = $AnimationPlayer
@onready var _canvas_layer = $Camera2D/CanvasLayer

var _move_dist = 0.0
var _last_move = Vector2.ZERO
var _marked_character = null
var _in_dialogue = false


func _ready() -> void:
	_detection_area.body_entered.connect(_on_body_entered)
	_detection_area.body_exited.connect(_on_body_exited)
	
	_animation_player.animation_finished.connect(_on_animation_finished)
	Dialogic.timeline_ended.connect(_on_timeline_ended)


func _process(__delta: float) -> void:
	if _in_dialogue:
		return
	
	var __move_vector = Vector2.ZERO
	if Input.is_action_pressed('player_move_up'):
		__move_vector.y -= 1
	elif Input.is_action_pressed('player_move_down'):
		__move_vector.y += 1
	if Input.is_action_pressed('player_move_left'):
		__move_vector.x -= 1
	elif Input.is_action_pressed('player_move_right'):
		__move_vector.x += 1
	
	move_and_collide(__move_vector.normalized() * MOVE_SPEED * __delta)
	_last_move = __move_vector
	
	var __current_pos = get_position()
	__current_pos.x = clamp(__current_pos.x, 90.0, 7720.0)
	__current_pos.y = clamp(__current_pos.y, 260.0, 4040.0)
	set_position(__current_pos)
	
	if Input.is_action_just_pressed('start_dialogue') and _marked_character != null:
		_animation_player.play('zoom_in')
		_canvas_layer.hide()
		_in_dialogue = true


func _physics_process(__delta) -> void:
	if _last_move == Vector2.ZERO:
		_move_dist = 0.0
		_sprite.set_position(SPRITE_POS)
	else:
		_move_dist += HOP_SPEED
		_sprite.set_position(SPRITE_POS - Vector2(0, abs(sin((_move_dist * __delta) / 30.0)) * 40.0))


func _on_body_entered(__body: Node2D) -> void:
	if 'rabbit_id' in __body:
		__body.mark_rabbit()
		_marked_character = __body


func _on_body_exited(__body: Node2D) -> void:
	if 'rabbit_id' in __body:
		__body.unmark_rabbit()
		_marked_character = __body


func _on_animation_finished(__anim_name: StringName) -> void:
	if __anim_name == 'zoom_in':
		Dialogic.start(STATE.get_available_story(_marked_character.rabbit_id))
	else:
		_in_dialogue = false
		_canvas_layer.show()


func _on_timeline_ended() -> void:
	_animation_player.play('zoom_out')
