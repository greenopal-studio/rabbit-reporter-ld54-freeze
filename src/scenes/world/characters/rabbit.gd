extends StaticBody2D

@export var rabbit_id: ID.CHARACTERS


@onready var _sprite_node = $Sprite2D
@onready var _rabbit_sprites = [
	preload('res://assets/sprites/rabbit2.png'),
	preload('res://assets/sprites/rabbit3.png'),
	preload('res://assets/sprites/rabbit4.png'),
	preload('res://assets/sprites/rabbit5.png'),
	preload('res://assets/sprites/rabbit6.png'),
	preload('res://assets/sprites/rabbit7.png'),
	preload('res://assets/sprites/rabbit8.png'),
	preload('res://assets/sprites/rabbit9.png'),
	preload('res://assets/sprites/rabbit10.png'),
	preload('res://assets/sprites/rabbit11.png'),
	preload('res://assets/sprites/rabbit12.png'),
	preload('res://assets/sprites/rabbit13.png'),
	preload('res://assets/sprites/rabbit14.png'),
	preload('res://assets/sprites/rabbit15.png'),
	preload('res://assets/sprites/rabbit16.png'),
	preload('res://assets/sprites/ball.png'),
]


func _ready():
	_sprite_node.set_offset(Vector2(0, -_rabbit_sprites[rabbit_id].get_height() / 2))
	_sprite_node.set_texture(_rabbit_sprites[rabbit_id])


func mark_rabbit() -> void:
	_sprite_node.set_use_parent_material(false)


func unmark_rabbit() -> void:
	_sprite_node.set_use_parent_material(true)
