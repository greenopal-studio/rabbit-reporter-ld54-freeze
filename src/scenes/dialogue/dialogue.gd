@tool
extends CanvasLayer

enum Alignments {LEFT, CENTER, RIGHT}

# Careful: Sync these with the ones in the %Animation script!
enum AnimationsIn {NONE, POP_IN, FADE_UP}
enum AnimationsOut {NONE, POP_OUT, FADE_DOWN}
enum AnimationsNewText {NONE, WIGGLE}

@export_group("Settings")
@export_subgroup("Box")
@export var box_animation_in := AnimationsIn.FADE_UP
@export var box_animation_out := AnimationsOut.FADE_DOWN
@export var box_animation_new_text := AnimationsNewText.NONE
@export_subgroup("Next Indicator")
@export var next_indicator_enabled := true
@export_enum('bounce', 'blink', 'none') var next_indicator_animation := 0
@export_file("*.png","*.svg") var next_indicator_texture := ''
@export var next_indicator_show_on_questions := true
@export var next_indicator_show_on_autoadvance := false


@export_subgroup("Indicators")
@export var autoadvance_progressbar := true


func _ready():
	if not Engine.is_editor_hint():
		$RootControl.theme = ThemeService.get_current_theme()


## Called by dialogic whenever export overrides might change
func _apply_export_overrides():
	if !is_inside_tree():
		await ready
	
	## BOX ANIMATIONS
	%Animations.animation_in = box_animation_in
	%Animations.animation_out = box_animation_out
	%Animations.animation_new_text = box_animation_new_text
	
	## NEXT INDICATOR SETTINGS
	if !next_indicator_enabled:
		%NextIndicator.queue_free()
	else:
		%NextIndicator.animation = next_indicator_animation
		if FileAccess.file_exists(next_indicator_texture):
			%NextIndicator.texture = load(next_indicator_texture)
		%NextIndicator.show_on_questions = next_indicator_show_on_questions
		%NextIndicator.show_on_autoadvance = next_indicator_show_on_autoadvance
	
	## OTHER
	%AutoAdvanceProgressbar.enabled = autoadvance_progressbar
