extends Control


signal closed()


func _ready():
	%Text.text = STATE.get_hint(SCENE.get_current_scene_id())

	%CloseButton.pressed.connect(_on_close_button_pressed)


func _on_close_button_pressed():
	if not STATE.get_has_seen_hint(SCENE.get_current_scene_id()):
		STATE.seen_hint.emit(SCENE.get_current_scene_id())

	closed.emit()
