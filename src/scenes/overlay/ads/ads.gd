extends Control


const SIZE = 250


signal selected(__ad_id: ID.ADS)
signal closed()


var _story_scene = preload('res://scenes/newspaper/newspaper_story.tscn')


@onready var _ads_container = %AdsFlowContainer
@onready var _empty_container = %EmptyContainer
@onready var _close_button = %CloseButton


func _ready():
	visibility_changed.connect(_on_visibility_changed)

	_close_button.pressed.connect(_on_close_pressed)


func _on_visibility_changed() -> void:
	if not visible:
		return

	for child in _ads_container.get_children():
		child.queue_free()

	var picked_ads := STATE.get_picked_ads()

	var has_added_ad := false

	for ad_data_key in DATA.ads:
		if ad_data_key in picked_ads:
			continue

		var ad_data = DATA.ads[ad_data_key]

		var ad_node = _story_scene.instantiate()
		_ads_container.add_child(ad_node)
		ad_node.is_ad = true
		ad_node.set_ad_texture(ad_data.get_texture())
		ad_node.width = SIZE
		ad_node.height = SIZE
		ad_node.pressed.connect(_on_ad_pressed.bind(ad_data_key))

		has_added_ad = true

	if has_added_ad:
		_empty_container.hide()
		_ads_container.show()
	else:
		_ads_container.hide()
		_empty_container.show()


func _on_ad_pressed(__ad_id: ID.ADS) -> void:
	selected.emit(__ad_id)
	closed.emit()


func _on_close_pressed() -> void:
	closed.emit()
