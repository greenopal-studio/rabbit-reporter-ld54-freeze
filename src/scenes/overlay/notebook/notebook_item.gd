extends Container

func set_data(text: String, character: String = "", character_color := Color()) -> void:
	%TextBox.text = text
	if character:
		%NameLabel.text = character
		%NameLabel.add_theme_color_override('font_color', character_color)
		%NameLabel.show()
	else:
		%NameLabel.hide()

func set_data_sub_entry(text: String) -> void:
	set_data(' » ' + text)
