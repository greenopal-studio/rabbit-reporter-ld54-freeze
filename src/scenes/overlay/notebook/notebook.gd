extends Control


signal closed()


var _notebook_item_scene = preload('res://scenes/overlay/notebook/notebook_item.tscn')


@onready var _notebook_log: VBoxContainer = %NotebookLog

@onready var _hide_notebook: Button = %HideNotebook


func _ready():
	visibility_changed.connect(_on_visibility_changed)

	_hide_notebook.pressed.connect(_on_hide_notebook_pressed)


func _on_visibility_changed() -> void:
	if not visible:
		return

	for child in _notebook_log.get_children():
		child.queue_free()

	for story_state in STATE.get_collected_stories():
		var story_data = DATA.stories[story_state.get_story_id()]
		var story_info_count = story_state.get_info_count()

		if story_info_count > 0:
			var item = _notebook_item_scene.instantiate()
			item.set_data(story_data.get_name())
			_notebook_log.add_child(item)

			for info_i in story_info_count:
				var sub_item = _notebook_item_scene.instantiate()
				sub_item.set_data_sub_entry(
					story_data.get_info_bullet_point_at_index(info_i)
				)
				_notebook_log.add_child(sub_item)


func _on_hide_notebook_pressed():
	closed.emit()
