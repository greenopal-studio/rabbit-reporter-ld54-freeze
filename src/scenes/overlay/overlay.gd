extends Control


signal ad_selected(__ad_id: ID.ADS)


@export var _enable_map := false
@export var _enable_notebook := false
@export var _enable_ads := false
@export var _enable_history := false

@onready var _day_label: Label = %DayLabel
@onready var _prestige_label: Label = %PrestigeLabel

@onready var _panel_container: PanelContainer = %OverlaysPanelContainer
@onready var _notebook_node: Control = %Notebook
@onready var _ads_node: Control = %Ads
@onready var _history_node: Control = %History
@onready var _hint_node: Control = %Hint

@onready var _show_map_button: Button = %ShowMap
@onready var _show_notebook_button: Button = %ShowNotebook
@onready var _show_ads_button: Button = %ShowAds
@onready var _show_history_button: Button = %ShowHistory
@onready var _show_hint_button: Button = %ShowHint


func _ready():
	_day_label.text = '%d' % STATE.get_day()
	_prestige_label.text = UtilService.format_count(STATE.get_prestige())

	_panel_container.hide()
	_notebook_node.hide()
	_ads_node.hide()
	_history_node.hide()
	_hint_node.hide()

	if _enable_map:
		_show_map_button.show()
		_show_map_button.pressed.connect(_on_show_map_button_pressed)
	else:
		_show_map_button.hide()

	if _enable_notebook:
		_show_notebook_button.show()
		_show_notebook_button.pressed.connect(_on_show_notebook_button_pressed)
		_notebook_node.closed.connect(_on_notebook_closed)
	else:
		_show_notebook_button.hide()

	if _enable_ads:
		_show_ads_button.show()
		_show_ads_button.pressed.connect(_on_show_ads_button_pressed)
		_ads_node.selected.connect(_on_ad_selected)
		_ads_node.closed.connect(_on_ads_closed)
	else:
		_show_ads_button.hide()

	if _enable_history:
		_show_history_button.show()
		_show_history_button.pressed.connect(_on_show_history_button_pressed)
		_history_node.closed.connect(_on_history_closed)
	else:
		_show_history_button.hide()

	if STATE.get_hint(SCENE.get_current_scene_id()):
		_show_hint_button.show()
		_show_hint_button.pressed.connect(_on_show_hint_button_pressed)
		_hint_node.closed.connect(_on_hint_closed)
		if not STATE.get_has_seen_hint(SCENE.get_current_scene_id()):
			_panel_container.show()
			_hint_node.show()
	else:
		_show_hint_button.hide()


func _on_show_map_button_pressed():
	SCENE.goto_scene(ID.SCENE_TYPES.WORLD_MAP)


func _on_show_notebook_button_pressed():
	_panel_container.show()
	_notebook_node.show()

	Dialogic.paused = true


func _on_notebook_closed():
	_panel_container.hide()
	_notebook_node.hide()

	Dialogic.paused = false
	

func _on_show_ads_button_pressed():
	_panel_container.show()
	_ads_node.show()

	Dialogic.paused = true


func _on_ad_selected(__ad_id: ID.ADS):
	ad_selected.emit(__ad_id)


func _on_ads_closed():
	_panel_container.hide()
	_ads_node.hide()

	Dialogic.paused = false	


func _on_show_history_button_pressed():
	_panel_container.show()
	_history_node.show()

	Dialogic.paused = true


func _on_history_closed():
	_panel_container.hide()
	_history_node.hide()

	Dialogic.paused = false


func _on_show_hint_button_pressed():
	_panel_container.show()
	_hint_node.show()

	Dialogic.paused = true


func _on_hint_closed():
	_panel_container.hide()
	_hint_node.hide()

	Dialogic.paused = false
