extends Button


signal headline_changed(__id: int)
signal format_changed()


@export var width: int = 300 :
	set(__width):
		if __width > 0:
			custom_minimum_size.x = __width
			size.x = __width
			size_flags_horizontal = SIZE_SHRINK_BEGIN
			size_flags_vertical = SIZE_SHRINK_BEGIN
	get:
		return int(custom_minimum_size.x)


@export var height: int = 300 :
	set(__height):
		if __height > 0:
			custom_minimum_size.y = __height
			size.y = __height
			size_flags_horizontal = SIZE_SHRINK_BEGIN
			size_flags_vertical = SIZE_SHRINK_BEGIN
	get:
		return int(custom_minimum_size.y)


@export var is_headline: bool = false :
	set(__is_headline):
		if has_node('%RegularBoxContainer') and has_node('%HeadlineBoxContainer'):
			%RegularBoxContainer.visible = not __is_headline
			%HeadlineBoxContainer.visible = __is_headline
	get:
		if has_node('%HeadlineBoxContainer'):
			return %HeadlineBoxContainer.visible
		else:
			return false


@export var is_ad: bool = false :
	set(__is_ad):
		if has_node('%AdContainer'):
			%AdContainer.visible = __is_ad
	get:
		if has_node('%AdContainer'):
			return %AdContainer.visible
		else:
			return false


@export var title: String = '' :
	set(__title):
		if has_node('%RegularTitleLabel') and has_node('%HeadlineTitleLabel'):
			%RegularTitleLabel.text = __title
			%HeadlineTitleLabel.text = __title
	get:
		if has_node('%RegularTitleLabel'):
			return %RegularTitleLabel.text
		else:
			return ''


@export var show_edit_overlay: bool = false :
	set(__visible):
		if has_node('%OverlayContainer'):
			%OverlayContainer.visible = __visible
	get:
		if has_node('%OverlayContainer'):
			return %OverlayContainer.visible
		return false


@export var show_change_format: bool = false :
	set(__visible):
		if has_node('%ChangeFormatContainer'):
			%ChangeFormatContainer.visible = __visible
	get:
		if has_node('%ChangeFormatContainer'):
			return %ChangeFormatContainer.visible
		return false


@export var show_shadow: bool = true :
	set(__visible):
		if has_node('%ShadowContainer'):
			%ShadowContainer.visible = __visible
	get:
		if has_node('%ShadowContainer'):
			return %ShadowContainer.visible
		return false


@export var rating: int = 5 :
	set(__value):
		if has_node('%RatingLabel'):
			%RatingLabel.text = '%d' % __value
			%RatingLabel.visible = true
			%RatingContainer.visible = true
	get:
		if has_node('%RatingLabel'):
			return int(%RatingLabel.text)
		return 0


func _ready():
	%HeadlineButton1.pressed.connect(_on_headline_button_pressed.bind(0))
	%HeadlineButton2.pressed.connect(_on_headline_button_pressed.bind(1))
	%HeadlineButton3.pressed.connect(_on_headline_button_pressed.bind(2))

	%ChangeFormatButton.pressed.connect(_on_format_changed)

	%OverlayContainer.hide()
	%ChangeFormatContainer.hide()
	%RatingContainer.hide()


func _on_headline_button_pressed(__id: int) -> void:
	headline_changed.emit(__id)


func _on_format_changed() -> void:
	format_changed.emit()


func set_ad_texture(__texture) -> void:
	%AdTextureRect.texture = __texture


func set_edit_headlines(__titles: Array) -> void:
	if len(__titles) >= 3:
		%HeadlineButton3.text = __titles[2]
		%HeadlineButton3.show()
	else:
		%HeadlineButton3.hide()
	
	if len(__titles) >= 2:
		%HeadlineButton2.text = __titles[1]
		%HeadlineButton2.show()
	else:
		%HeadlineButton2.hide()

	%HeadlineButton1.text = __titles[0]
