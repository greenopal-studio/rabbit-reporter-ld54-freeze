extends PanelContainer


var command_entry_template = preload('res://scenes/dev_console/dev_console_entry.tscn')


@onready var popup_panel = $'%PopupPanel'
@onready var command_container = $'%CommandContainer'
@onready var command_edit: LineEdit = $'%CommandEdit'


var _logger: Logger = LOGGING.get_logger('DevConsole')


func _ready():
	hide()
	popup_panel.hide()

	z_index = RenderingServer.CANVAS_ITEM_Z_MAX

	popup_panel.popup_hide.connect(_on_hide_popup_panel)
	command_edit.text_submitted.connect(_on_text_entered)

	command_edit.text = ''


func popup():
	show()
	popup_panel.popup()
	command_edit.grab_focus()


func _on_hide_popup_panel():
	hide()
	_fade_last_entry()


func _on_text_entered(__raw_command: String):
	_logger.info('Executed command %s'%[__raw_command])

	var split_command = __raw_command.split(' ')
	var command = '__cmd_%s'%split_command[0]
	var args = split_command.slice(1)

	var new_entry = command_entry_template.instantiate()

	var success = true
	if has_method(command):
		var result = call(command, args)
		if result:
			if typeof(result) == TYPE_STRING:
				new_entry.set_data_result(__raw_command, result)
			elif result.has('is_error'):
				new_entry.set_data_error(__raw_command, result.get('message', 'Error'))
				success = false
			else:
				new_entry.set_data_result(__raw_command, result.get('message', 'ok...'))
		else:
			new_entry.set_data_ok(__raw_command)
	else:
		new_entry.set_data_error(__raw_command, 'Command not defined!')
		success = false

	if success:
		command_edit.text = ''

	_fade_last_entry()
	command_container.add_child(new_entry)
	command_container.move_child(new_entry, 0)


func _fade_last_entry():
	if command_container.get_child_count() > 0:
		command_container.get_child(0).modulate = Color(1, 1, 1, 0.6)


func __cmd_clear(__args):
	for child in command_container.get_children():
		child.queue_free()


func __cmd_help(__args):
	var command_list: String = 'available commands:'
	for method in get_method_list():
		if method.name.begins_with('__cmd_'):
			command_list += '\n  ' + method.name.trim_prefix('__cmd_')
	return command_list


# Add custom commands below:


func __cmd_echo(__args):
	if __args.size() != 1:
		return { is_error=true, message='invalid arguments! expected: <text>' }

	return __args[0]


func __cmd_get_save_id(__args):
	var id = STATE.get_current_save_id()
	return id if id else '<no save loaded>'
