extends Control


@onready var _world_sub_viewport = %AssembleWorldSubViewport
@onready var _world_node = %AssembleWorld
@onready var _overlay_node = %Overlay



func _ready():
	_overlay_node.ad_selected.connect(_on_ad_selected)
	%PublishButton.pressed.connect(_on_publish_button_pressed)


func _input(event):
	_world_sub_viewport.push_input(event)


func _on_ad_selected(__ad_id: ID.ADS) -> void:
	STATE.pick_ad.emit(__ad_id)
	_world_node.pick_ad(__ad_id)


func _on_publish_button_pressed() -> void:
	SCENE.goto_scene(ID.SCENE_TYPES.RATING)
