@tool
extends Node2D


signal headline_changed(__id: int)
signal format_changed()


@export var width: int = 300 :
	set(__width):
		if __width > 0 and has_node('StorySubViewport'):
			$StorySubViewport.size.x = __width
			$'StorySubViewport/NewspaperStory'.size.x = __width
	get:
		if has_node('StorySubViewport'):
			return $StorySubViewport.size.x
		else:
			return 0


@export var height: int = 200 :
	set(__height):
		if __height > 0 and has_node('StorySubViewport'):
			$StorySubViewport.size.y = __height
			$'StorySubViewport/NewspaperStory'.size.y = __height
	get:
		if has_node('StorySubViewport'):
			return $StorySubViewport.size.y
		else:
			return 0


@export var is_headline: bool = false :
	set(__is_headline):
		if has_node('StorySubViewport/NewspaperStory'):
			$'StorySubViewport/NewspaperStory'.is_headline = __is_headline
	get:
		if has_node('StorySubViewport/NewspaperStory'):
			return $'StorySubViewport/NewspaperStory'.is_headline
		else:
			return false


@export var is_ad: bool = false :
	set(__is_ad):
		if has_node('StorySubViewport/NewspaperStory'):
			$'StorySubViewport/NewspaperStory'.is_ad = __is_ad
	get:
		if has_node('StorySubViewport/NewspaperStory'):
			return $'StorySubViewport/NewspaperStory'.is_ad
		else:
			return false


@export var show_edit_overlay: bool = false :
	set(__show_edit_overlay):
		if has_node('StorySubViewport/NewspaperStory'):
			$'StorySubViewport/NewspaperStory'.show_edit_overlay = __show_edit_overlay
	get:
		if has_node('StorySubViewport/NewspaperStory'):
			return $'StorySubViewport/NewspaperStory'.show_edit_overlay
		else:
			return false


@export var show_change_format: bool = false :
	set(__show_change_format):
		if has_node('StorySubViewport/NewspaperStory'):
			$'StorySubViewport/NewspaperStory'.show_change_format = __show_change_format
	get:
		if has_node('StorySubViewport/NewspaperStory'):
			return $'StorySubViewport/NewspaperStory'.show_change_format
		else:
			return false


@export var title: String = '' :
	set(__title):
		if has_node('StorySubViewport/NewspaperStory'):
			$'StorySubViewport/NewspaperStory'.title = __title
	get:
		if has_node('StorySubViewport/NewspaperStory'):
			return $'StorySubViewport/NewspaperStory'.title
		else:
			return ''


@export var rating: int = -1
@export var size_grid: Vector2 = Vector2.ZERO
@export var story_id: ID.STORIES = -1
@export var ad_id: ID.ADS = -1


@onready var _story_node = $'StorySubViewport/NewspaperStory'

func _ready():
	_story_node.headline_changed.connect(_on_headline_changed)
	_story_node.format_changed.connect(_on_format_changed)

	_story_node.size.x = $StorySubViewport.size.x
	_story_node.size.y = $StorySubViewport.size.y


func push_input(event):
	var dup_event = event.duplicate(true)
	# quick tricks to fix my completely mis-judged decision to use viewports inside a 2D world...
	if 'position' in dup_event:
		dup_event.position -= position
	$StorySubViewport.push_input(dup_event)


func _on_headline_changed(__id: int) -> void:
	headline_changed.emit(__id)


func _on_format_changed() -> void:
	format_changed.emit()


func set_ad_texture(__texture) -> void:
	_story_node.set_ad_texture(__texture)

func set_edit_headlines(__titles: Array) -> void:
	_story_node.set_edit_headlines(__titles)
