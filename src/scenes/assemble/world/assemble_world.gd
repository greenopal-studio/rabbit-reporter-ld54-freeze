extends Node2D


const NEWSPAPER_GRID_COUNT_X = 3
const NEWSPAPER_GRID_COUNT_Y = 4


@onready var _newspaper_background: Polygon2D = %NewspaperBackground
@onready var _stories_anchor: Node2D = %StoriesAnchor
@onready var _error_anchor: Node2D = %ErrorTextAnchor
@onready var D1 = %D1
@onready var D2 = %D2
@onready var D3 = %D3


var _story_scene = preload('res://scenes/assemble/world/assemble_story.tscn')


var _last_mouse_pos: Vector2 = Vector2.INF
var _moving_story: Node = null
var _moving_story_offset: Vector2 = Vector2.INF
var _last_moved_pos_grid: Vector2 = Vector2.INF
var _last_valid_pos: Vector2 = Vector2.INF

var _newspaper_start: Vector2
var _newspaper_end: Vector2
var _newspaper_grid_x: int
var _newspaper_grid_y: int

var _spawn_top_center: Vector2


func _ready():
	var newspaper_polygon = _newspaper_background.polygon
	_newspaper_start = newspaper_polygon[0]
	_newspaper_end = newspaper_polygon[2]
	var newspaper_width = newspaper_polygon[3].x - newspaper_polygon[0].x
	_newspaper_grid_x = int(newspaper_width / NEWSPAPER_GRID_COUNT_X)
	var newspaper_height = newspaper_polygon[1].y - newspaper_polygon[0].y
	_newspaper_grid_y = int(newspaper_height / NEWSPAPER_GRID_COUNT_Y)

	_spawn_top_center = Vector2(
		_newspaper_start.x / 2,
		_newspaper_start.y
	)

	for collected_story_i in len(STATE.get_collected_stories()):
		var collected_story = STATE.get_collected_stories()[collected_story_i]
		var story_id = collected_story.get_story_id()
		var info_count = collected_story.get_info_count()
		var story_data = DATA.stories[story_id]

		var size_grid = Vector2.ONE
		match info_count:
			2:
				size_grid = Vector2(2, 1)
			3:
				size_grid = Vector2(3, 2)

		var width = size_grid.x * _newspaper_grid_x
		var height = size_grid.y * _newspaper_grid_y

		var pos = (
			_spawn_top_center
			- Vector2(width / 2, 0)
			+ Vector2(0, (collected_story_i * _newspaper_grid_y) * 0.4)
			+ _get_randomized_spawn_offset()
		)

		var unlocked_headlines = story_data.get_headlines()[info_count - 1]

		var title_data = UtilService.pick_random_stateful(
			unlocked_headlines
		)
		var title = title_data[0]
		var rating = title_data[1]

		var story_node = _story_scene.instantiate()
		_stories_anchor.add_child(story_node)

		story_node.width = width
		story_node.height = height
		story_node.size_grid = size_grid
		story_node.position = pos
		story_node.story_id = story_id
		story_node.is_headline = info_count == 3
		story_node.show_change_format = info_count == 2
		story_node.title = title
		story_node.rating = rating

		story_node.set_edit_headlines(
			unlocked_headlines.map(func(data): return data[0])
		)

		story_node.headline_changed.connect(_on_story_headline_changed.bind(story_node))
		story_node.format_changed.connect(_on_story_format_changed.bind(story_node))


func _input(event):
	var stories_children_rev = Array(_stories_anchor.get_children())
	stories_children_rev.reverse()

	if event is InputEventMouseMotion:
		if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT):
			if not _moving_story:
				for child in stories_children_rev:
					if UtilService.is_pos_inside_rect(
						event.position, 
						child.position, 
						child.position + Vector2(child.width, child.height)
					):
						_moving_story = child
						_moving_story_offset = event.position - child.position
						_stories_anchor.move_child(_moving_story, -1)
						break
			elif (_last_mouse_pos - event.position).length_squared() > 25:
				_moving_story.show_edit_overlay = false

				var story_size = Vector2(_moving_story.width, _moving_story.height)

				var offset_pos = event.position - _moving_story_offset - _newspaper_start
				if offset_pos.x < 0 and abs(offset_pos.x) > _newspaper_grid_x * 0.5:
					offset_pos.x += _newspaper_grid_x * 0.5

				var snapped_pos = Vector2(
					min(
						_newspaper_end.x - _moving_story.width,
						_newspaper_start.x + snappedi(
							offset_pos.x,
							_newspaper_grid_x
						)
					),
					max(0, min(
						_newspaper_end.y - _moving_story.height,
						_newspaper_start.y + snappedi(
							offset_pos.y,
							_newspaper_grid_y
						)
					))
				)

				D1.position = snapped_pos
				D2.position = event.position
				D3.position = event.position - _moving_story_offset

				if (
					UtilService.is_pos_inside_rect(
						snapped_pos, _newspaper_start, _newspaper_end
					) and UtilService.is_pos_inside_rect(
						snapped_pos + story_size, _newspaper_start, _newspaper_end
					)
				):
					var snapped_pos_grid = Vector2(
						int((snapped_pos.x - _newspaper_start.x) / _newspaper_grid_x),
						int((snapped_pos.y - _newspaper_start.y) / _newspaper_grid_y)
					)

					var ok = true
					for placed_story in STATE.get_placed_stories():
						if (
							(
								# placed story is not the same story/ad:
								(
									_moving_story.story_id != -1
									and _moving_story.story_id != placed_story.get_used_story_id()
								) or (
									_moving_story.ad_id != -1
									and _moving_story.ad_id != placed_story.get_used_ad_id()
								)
							) and (
								UtilService.is_pos_inside_rect(
									snapped_pos_grid,
									placed_story.get_pos(),
									placed_story.get_pos() + placed_story.get_size() - Vector2.ONE
								) or (
									_moving_story.size_grid.length_squared() > 1
									and UtilService.is_pos_inside_rect(
										snapped_pos_grid + _moving_story.size_grid - Vector2.ONE,
										placed_story.get_pos(),
										placed_story.get_pos() + placed_story.get_size() - Vector2.ONE
									)
								) or UtilService.is_pos_inside_rect(
									placed_story.get_pos(),
									snapped_pos_grid,
									snapped_pos_grid + _moving_story.size_grid - Vector2.ONE
								) or (
									placed_story.get_size().length_squared() > 1
									and UtilService.is_pos_inside_rect(
										placed_story.get_pos() + placed_story.get_size() - Vector2.ONE,
										snapped_pos_grid,
										snapped_pos_grid + _moving_story.size_grid - Vector2.ONE
									)
								)
							)
						):
							ok = false
							break

					if ok:
						_moving_story.position = snapped_pos
						_last_valid_pos = snapped_pos
						_last_moved_pos_grid = snapped_pos_grid
						_error_anchor.hide()
					else:
						_moving_story.position = event.position - _moving_story_offset
						_error_anchor.show()
						_error_anchor.position = event.position - _moving_story_offset + (
							story_size / 2
						)

				elif event.position.x < _newspaper_start.x:
					_moving_story.position = event.position - _moving_story_offset
					_last_valid_pos = _moving_story.position
					_last_moved_pos_grid = Vector2.INF
					_error_anchor.hide()

			_last_mouse_pos = event.position

		else:
			var found = false
			for child in stories_children_rev:
				if (
					not found
					and UtilService.is_pos_inside_rect(
						event.position, 
						child.position, 
						child.position + Vector2(child.width, child.height)
					)
				):
					found = true
					if child.story_id != -1:
						child.show_edit_overlay = true
					else:
						child.show_edit_overlay = false # = ad
				else:
					child.show_edit_overlay = false
		
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT and not event.pressed:
		if _moving_story:
			if _last_valid_pos != Vector2.INF:
				_moving_story.position = _last_valid_pos
			else:
				_moving_story.position = Vector2.ZERO

			_error_anchor.hide()

			if _moving_story.story_id != -1:
				if _last_moved_pos_grid != Vector2.INF:
					STATE.place_story.emit(
						_moving_story.story_id, _moving_story.title, _moving_story.rating,
						_last_moved_pos_grid, _moving_story.size_grid, _moving_story.is_headline
					)
				else:
					STATE.remove_placed_story.emit(_moving_story.story_id)
			elif _moving_story.ad_id != -1:
				if _last_moved_pos_grid != Vector2.INF:
					STATE.place_ad.emit(
						_moving_story.ad_id, _last_moved_pos_grid
					)
				else:
					STATE.remove_placed_ad.emit(_moving_story.ad_id)

			_last_mouse_pos = Vector2.INF
			_moving_story = null
			_moving_story_offset = Vector2.INF
			_last_moved_pos_grid = Vector2.INF
			_last_valid_pos = Vector2.INF

	if event is InputEventMouse:
		for child in stories_children_rev:
			if UtilService.is_pos_inside_rect(
				event.position, 
				child.position, 
				child.position + Vector2(child.width, child.height)
			):
				child.push_input(event)
				break


func pick_ad(__ad_id: ID.ADS):
	var ad_data = DATA.ads[__ad_id]

	var ad_node = _story_scene.instantiate()
	_stories_anchor.add_child(ad_node)
	ad_node.width = _newspaper_grid_x * 1
	ad_node.height = _newspaper_grid_y * 1
	ad_node.position = (
		_spawn_top_center
		+ Vector2(_newspaper_grid_x * -2, _newspaper_grid_y * 0.5)
		+ _get_randomized_spawn_offset()
	)
	ad_node.is_ad = true
	ad_node.ad_id = __ad_id
	ad_node.set_ad_texture(ad_data.get_texture())


func _on_story_headline_changed(__headline_idx: int, __story_node) -> void:
	var story_data = DATA.stories[__story_node.story_id]
	var collected_story = STATE.get_collected_story(__story_node.story_id)
	var info_count = collected_story.get_info_count()

	var headline_data = collected_story.get_headlines()[info_count - 1][__headline_idx]
	var title = headline_data[0]
	var rating = headline_data[1]

	__story_node.title = title
	__story_node.rating = rating

	for placed_story in STATE.get_placed_stories():
		if placed_story.get_used_story_id() == __story_node.story_id:
			STATE.place_story.emit(
				__story_node.story_id, title, rating,
				placed_story.get_pos(), placed_story.get_size(), __story_node.is_headline
			)
			break


func _on_story_format_changed(__story_node) -> void:
	var new_size_grid = Vector2(
		__story_node.size_grid.y,
		__story_node.size_grid.x
	)
	__story_node.size_grid = new_size_grid
	__story_node.width = new_size_grid.x * _newspaper_grid_x
	__story_node.height = new_size_grid.y * _newspaper_grid_y

	for placed_story in STATE.get_placed_stories():
		if placed_story.get_used_story_id() == __story_node.story_id:
			STATE.place_story.emit(
				__story_node.story_id, __story_node.title, __story_node.rating,
				placed_story.get_pos(), new_size_grid, __story_node.is_headline
			)
			break


func _get_randomized_spawn_offset() -> Vector2:
	return Vector2(
		STATE.rng.randi_range(
			int(_newspaper_grid_x * -0.125),
			int(_newspaper_grid_x * 0.125)
		),
		STATE.rng.randi_range(0, int(_newspaper_grid_y * 0.05))
	)
