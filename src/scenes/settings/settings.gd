class_name SettingsScene extends Control


@onready var _save_button: Button = $'%SaveButton'
@onready var _reset_button: Button = $'%ResetButton'
@onready var _entries_node: VBoxContainer = $'%Entries'


var _current_settings: SettingsData
var _changed_settings: Array[int] = []


func _ready():
	$'%BackButton'.pressed.connect(_on_back_to_menu_pressed)
	_save_button.pressed.connect(_on_save_and_back_pressed)
	_reset_button.pressed.connect(_on_reset_pressed)
	
	_current_settings = SETTINGS.get_data(true)
	
	for __entry_node in _entries_node.get_children():
		__entry_node.set_data(_current_settings, self)


func add_setting_changed(__setting_type: int) -> void:
	_changed_settings.append(__setting_type)
	if _changed_settings.size() > 0:
		_save_button.set_disabled(false)


func remove_setting_changed(__setting_type: int) -> void:
	_changed_settings.erase(__setting_type)
	if _changed_settings.size() == 0:
		_save_button.set_disabled(true)


func _on_save_and_back_pressed() -> void:
	SETTINGS.set_data(_current_settings, _changed_settings)
	_on_back_to_menu_pressed()


func _on_back_to_menu_pressed() -> void:
	SCENE.goto_scene(ID.SCENE_TYPES.MENU)


func _on_reset_pressed() -> void:
	SETTINGS.set_data(SettingsData.new(), ID.SETTING_TYPES.values())
	SCENE.goto_scene(ID.SCENE_TYPES.MENU)
