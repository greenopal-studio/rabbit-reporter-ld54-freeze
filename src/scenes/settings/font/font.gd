extends HBoxContainer


@onready var _font_scale_slider: HSlider = $'%FontScaleSlider'

var _current_settings: SettingsData
var _settings_scene: SettingsScene


func _ready():
	_font_scale_slider.value_changed.connect(_on_font_scale_changed)

	# to keep the min and max size defined in ThemeService as the definitive setting, we set the
	#  min and max value of the slider at runtime:
	_font_scale_slider.min_value = (
		float(ThemeService.min_font_size)
		/ float(ThemeDB.get_project_theme().get_default_font_size())
	)
	_font_scale_slider.max_value = (
		float(ThemeService.max_font_size)
		/ float(ThemeDB.get_project_theme().get_default_font_size())
	)


func set_data(__current_settings: SettingsData, __settings_scene: SettingsScene) -> void:
	_current_settings = __current_settings
	_settings_scene = __settings_scene
	
	_set_font_scale(_current_settings.get_font_scale())


func _on_font_scale_changed(__new_value: float) -> void:
	if not _current_settings:
		return
	
	var __was_different = \
		_current_settings.get_font_scale() != SETTINGS.get_data().get_font_scale()
	
	var __was_changed = _current_settings.set_font_scale(__new_value)
	if __was_changed:
		ThemeService.set_font_scale(__new_value)
		if _current_settings.get_font_scale() == SETTINGS.get_data().get_font_scale():
			_settings_scene.remove_setting_changed(ID.SETTING_TYPES.FONT_SCALE)
		elif not __was_different:
			_settings_scene.add_setting_changed(ID.SETTING_TYPES.FONT_SCALE)


func _set_font_scale(__new_value: float) -> void:
	_font_scale_slider.value = __new_value
