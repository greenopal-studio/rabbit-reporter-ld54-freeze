extends Control


func _ready():
	%TestDialogueButton.pressed.connect(_on_test_dialogue_button_pressed)
	%TestAssembleButton.pressed.connect(_on_test_assemble_button_pressed)

	STATE._collected_stories.append(
		CollectedStoryState.new(
			ID.STORIES.TEST_1,
			0
		)
	)
	STATE._collected_stories.append(
		CollectedStoryState.new(
			ID.STORIES.TEST_2,
			0
		)
	)
	STATE._collected_stories.append(
		CollectedStoryState.new(
			ID.STORIES.TEST_3,
			0
		)
	)

	STATE._collected_stories.append(
		CollectedStoryState.new(
			ID.STORIES.VEGETARIAN_OPTIONS,
			0
		)
	)
	Dialogic.VAR.set_variable(
		DATA.DIALOGIC_STORIES_VAR_PREFIX + DATA.stories[ID.STORIES.VEGETARIAN_OPTIONS].get_dialogic_var_name(),
		1
	)


func _on_test_dialogue_button_pressed() -> void:
	hide()
	Dialogic.start('grandma')
	await Dialogic.timeline_ended
	show()


func _on_test_assemble_button_pressed() -> void:
	SCENE.goto_scene(ID.SCENE_TYPES.ASSEMBLE_NEWSPAPER)
