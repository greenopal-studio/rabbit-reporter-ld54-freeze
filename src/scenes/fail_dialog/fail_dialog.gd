extends Control


@onready var okay_button = $'%OkayButton'
@onready var text_label = $'%TextLabel'


func _ready():
	okay_button.pressed.connect(func(): queue_free())
	
	
func set_text(__text: String):
	text_label.set_text(__text)
