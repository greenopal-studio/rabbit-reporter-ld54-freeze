class_name SerializeService


# SERIALIZE:

## Save __data with name taken from the __meta.
## Returns the id ("name_timestamp") which was generated for the save.
static func save_data_with_generated_id(
	__sub_path: String, __version: int, __meta: Dictionary, __data: Variant
) -> String:
	ensure_sub_path_exists(__sub_path)

	var __name = 'untitled'
	if 'name' in __meta:
		var regex = RegEx.new()
		regex.compile('[^a-zA-Z0-9-_]')
		__name = regex.sub(__meta['name'].replace(' ', '-'), '', true)
	
	__meta['date'] = Time.get_datetime_string_from_system().replace(':', '-').replace('T', '_')

	var __id = '%s_%s'%[
		__name, __meta['date']
	]
	save_data_with_id(__sub_path, __id, __version, __meta, __data)
	return __id


static func save_data_with_id(
	__sub_path: String, __id: String, __version: int, __meta: Dictionary, __data: Variant
) -> void:
	var save_path = generate_save_file_name(__sub_path, __id)
	save_data_with_full_path(save_path, __version, __meta, __data)


static func save_data_with_full_path(
	__full_path: String, __version: int, __meta: Dictionary, __data: Variant
) -> void:
	if not __full_path.contains('://'):
		__full_path = 'user://' + __full_path

	_get_logger().info('Started saving __data to %s'%[__full_path])

	var data_str = stringify_data(__version, __meta, __data)
	var save_file = FileAccess.open(__full_path, FileAccess.WRITE)
	save_file.store_line(data_str)
	save_file.close()

	_get_logger().info('Finished saving __data to %s'%[__full_path])


static func stringify_data(
	__version: int, __meta: Dictionary, __data: Variant
) -> String:
	return JSON.stringify({
		version = __version,
		meta = __meta,
		data = variant_to_serializable(__data)
	}, '', false)


static func variant_to_serializable(
	__value: Variant
) -> Variant:
	var type = typeof(__value)
	match type:
		TYPE_NIL:
			return null
		TYPE_BOOL, TYPE_FLOAT, TYPE_STRING, TYPE_INT:
			return __value
		TYPE_VECTOR2:
			return [ __value.x, __value.y ]
		TYPE_VECTOR3:
			return [ __value.x, __value.y, __value.z ]
		TYPE_VECTOR4:
			return [ __value.x, __value.y, __value.z, __value.w ]
		TYPE_COLOR:
			return [ __value.r, __value.g, __value.b, __value.a ]
		TYPE_ARRAY:
			var result = []
			for entry in __value:
				result.append(variant_to_serializable(entry))
			return result
		TYPE_DICTIONARY:
			var result = {}
			for entry_key in __value.keys():
				result[entry_key] = variant_to_serializable(__value[entry_key])
			return result
		TYPE_OBJECT:
			if __value.get_class() == 'RandomNumberGenerator':
				return [ __value.get_seed(), __value.get_state() ]
			else:
				var result = {}
				for prop in __value.get_property_list():
					if prop['name'].begins_with('__'):
						continue
					if prop['name'] == '_logger':
						continue
					if prop['usage'] == PROPERTY_USAGE_SCRIPT_VARIABLE:
						result[prop.name] = variant_to_serializable(__value.get(prop.name))
				return result
		_:
			_get_logger().error('The type %s is not supported (yet) by AdvancedJsonService!'%type)
			return null


# DE-SERIALIZE:

static func load_meta_with_id(
	__sub_path: String, __id: String
) -> Variant:
	var full_path = generate_save_file_name(__sub_path, __id)

	if not FileAccess.file_exists(full_path):
		_get_logger().error('Error while trying to load %s: File does not exist'%full_path)
		return null

	var save_file := FileAccess.open(full_path, FileAccess.READ)
	var save_string := save_file.get_as_text()
	save_file.close()

	var save_json := JSON.new()
	var json_parse_result := save_json.parse(save_string)
	if not json_parse_result == OK:
		_get_logger().error("Error while trying to parse save: %s"%save_json.get_error_message())
		return null

	var save_dict: Dictionary = save_json.get_data()

	if 'meta' not in save_dict:
		_get_logger().error('Save is missing the meta property!')
		return null

	return save_dict['meta']


static func load_data_with_id(
	__sub_path: String, __id: String, __base_class: Variant
) -> Variant:
	var full_path = generate_save_file_name(__sub_path, __id)
	return load_data(full_path, __base_class)


static func load_data_with_id_into_object(
	__sub_path: String, __id: String, __base_class: Variant, target: Variant
) -> bool:
	var full_path = generate_save_file_name(__sub_path, __id)
	var parsed_data = load_data(full_path, __base_class)
	if parsed_data == null:
		return false
	assign_script_variables(parsed_data, target)
	return true


static func load_data(
	__full_path: String, __base_class: Variant
) -> Variant:
	if not __full_path.contains('://'):
		__full_path = 'user://' + __full_path

	if not FileAccess.file_exists(__full_path):
		_get_logger().error('Error while trying to load %s: File does not exist'%__full_path)
		return null

	var save_file := FileAccess.open(__full_path, FileAccess.READ)
	var save_string := save_file.get_as_text()
	save_file.close()

	return parse_save_string(save_string, __base_class)


static func parse_save_string(
	__save_string: String, __base_class: Variant = null
) -> Variant:
	var save_json := JSON.new()
	var save_parse_result := save_json.parse(__save_string)
	if not save_parse_result == OK:
		_get_logger().error("Error while trying to parse passed __data string: %s"%save_json.get_error_message())
		return null

	var save_dict: Dictionary = save_json.get_data()

	if 'version' not in save_dict:
		_get_logger().error('Save is missing the version property!')
		return null
	if 'data' not in save_dict:
		_get_logger().error('Save is missing the data property!')
		return null

	return obj_from_serialized(save_dict.version as int, save_dict.data as Dictionary, __base_class)


static func variant_from_serialized(
	__version: int, __value: Variant, __base_class: Variant = null
) -> Variant:
	match typeof(__base_class):
		TYPE_NIL:
			return null
		TYPE_BOOL, TYPE_FLOAT, TYPE_STRING:
			return __value
		TYPE_INT:
			return int(__value)
		TYPE_VECTOR2:
			return Vector2(__value[0], __value[1])
		TYPE_VECTOR3:
			return Vector3(__value[0], __value[1], __value[2])
		TYPE_VECTOR4:
			return Vector4(__value[0], __value[1], __value[2], __value[3])
		TYPE_COLOR:
			return Color(__value[0], __value[1], __value[2], __value[3])
		TYPE_ARRAY:
			var result = []
			for entry in __value:
				result.append(variant_from_serialized(__version, entry, __base_class))
			return result
		TYPE_DICTIONARY:
			var __value_as_dict = __value as Dictionary
			var result = {}
			for key in __value_as_dict.keys():
				result[key] = variant_from_serialized(__version, __value_as_dict[key], __base_class)
			return result
		TYPE_OBJECT:
			return obj_from_serialized(__version, __value as Dictionary, __base_class)
		_:
			_get_logger().error('The type %s is not supported by AdvancedJsonService!'%typeof(__base_class))
			return null


static func obj_from_serialized(
	__version: int, __obj_dict: Dictionary, __base_class: Variant = null
) -> Variant:
	if not __base_class:
		_get_logger().error('No __base_class was set, so we can\'t get the type of properties!')
		return null

	var result: Object = __base_class.new()

	if result.has_method('reset'):
		result.reset()

	if result.has_method('migrate'):
		result.migrate(__version, __obj_dict)

	for key in __obj_dict.keys():
		if not key in result:
			_get_logger().error(
				'The class %s does not have a property %s!'%[ __base_class, key ] +
				' The migrate() func needs to be extended.'
			)
			continue

		var entry_serialized = __obj_dict[key]
		var entry_type = typeof(result[key])

		match entry_type:
			TYPE_ARRAY, TYPE_DICTIONARY, TYPE_OBJECT:
				if typeof(result[key]) == TYPE_OBJECT and result[key].get_class() == 'RandomNumberGenerator':
					var rng = RandomNumberGenerator.new()
					rng.set_seed(int(entry_serialized[0]))
					rng.set_state(int(entry_serialized[1]))
					result.set(key, rng)
					continue

				if !result.has_method('get_type_hints'):
					_get_logger().error(
						'The class %s does not define a get_type_hint() func!'%[ __base_class, key ] +
						' It needs to be added.'
					)
					result.set(key, null)
					continue

				var type_hints = __base_class.get_type_hints()
				if not key in type_hints:
					_get_logger().error(
						'The class %s does not define a class for property %s!'%[ __base_class, key ] +
						' The get_type_hint() func needs to be extended.'
					)
					result.set(key, null)
					continue
				var sub_class = __base_class.get_type_hints()[key]

				if entry_type == TYPE_ARRAY:
					var entry_result = []
					for sub_entry in entry_serialized:
						entry_result.append(variant_from_serialized(__version, sub_entry, sub_class))
					# As we want to have fully typed arrays, the runtime type-check complains at a
					#  direct Object.set, because our parsing returns a variant array instead of
					#  a typed array. Array.assign however is nice enough to coerce our type, so
					#  we need to use that instead.
					result[key].clear()
					result[key].assign(entry_result)
				elif entry_type == TYPE_DICTIONARY:
					var entry_result = {}
					for sub_entry_key in entry_serialized.keys():
						var target_sub_entry_key = sub_entry_key
						# Coerce int-values into actually int - needed for compatibility with Enum
						#  values as Dict keys:
						if sub_entry_key.is_valid_int():
							target_sub_entry_key = int(sub_entry_key)
						entry_result[target_sub_entry_key] = variant_from_serialized(
							__version, entry_serialized[sub_entry_key], sub_class
						)
					result.set(key, entry_result)
				else: # TYPE_OBJECT
					result.set(key, obj_from_serialized(__version, entry_serialized, sub_class))

			_:
				result.set(key, variant_from_serialized(__version, entry_serialized, result[key]))

	return result


# HELPER METHODS:

static func ensure_sub_path_exists(
	__sub_path: String
) -> void:
	var dir_path: String = 'user://%s'%__sub_path

	var dir = DirAccess.open('user://')
	if not dir.dir_exists(dir_path):
		dir.make_dir(dir_path)


static func generate_save_file_name(
	__sub_path: String, __id: String
) -> String:
	return 'user://%s/%s.save'%[__sub_path, __id]


static func assign_script_variables(
	__source: Variant, __target: Variant
) -> void:
	for prop_dict in __target.get_property_list():
		if prop_dict['usage'] == PROPERTY_USAGE_SCRIPT_VARIABLE:
			__target[prop_dict['name']] = __source[prop_dict['name']]


static func _get_logger() -> Logger:
	return LOGGING.get_logger('SerializeService')
