extends Node


const DIALOGIC_STORIES_VAR_PREFIX = 'stories.'
const DIALOGIC_HEADLINE_APPROVAL_VAR_PREFIX = 'headline_approval.'


var hints = {
	ID.SCENE_TYPES.WORLD_MAP: """
Hello Mr. R. Reporter and welcome to your internship at \"Hopping News\"!

This view is the map, which lets you [i]hop[/i] to any location in your neighborhood.
After you have selected a location, you can collect information for stories from your fellow rabbits by interviewing them.

Once you collected enough stories to fill your local news page in our newspaper, you should [i]hop[/i] to the [b][i]Office[/i][/b] (but only then, we don't want to see you empty-pawed).

Good luck and have fun!
	""",
	ID.SCENE_TYPES._ANY_LOCATION: """
You made it to your first location!

Now move around with [b][ W ] / [ A ] / [ S ] / [ D ][/b] or with the [b]arrow keys[/b] and approach a fellow rabbit.

Once you invaded their personal space close enough to interview them, they will be marked in [color=yellow]yellow[/color]!
Then press [b][ Enter ] / [ Space ] / [ E ][/b] to start the interview.

While interviewing them, different buttons appear in the middle of your screen to select what you want to ask the other rabbit.
Be sure to really dig deep to get all the interesting details out of them.

After you have interviewed some rabbits, click that handy [b][i]Map[/i][/b] button to go back again.

Let's go!
	""",
	ID.SCENE_TYPES.ASSEMBLE_NEWSPAPER: """
Welcome back to the office, here is your cubicle.

Now that you collected some stories, it's time to assemble them into a page for the local part of our newspaper.

But be warned, you only have [wave][rainbow][i]limited space[/i][/rainbow][/wave] to fit all stories onto the page.
Whoa, how did my voice do that?
[wave][rainbow][i]L I M I T E D   S P A C E  ![/i][/rainbow][/wave]

Anyway, when you hover over a story, you can choose between different options for how to write the headline, if possible.
Then click and drag the stories onto the template on the right.

Due to the [wave]peculiar financial situation[/wave] of our newspaper, I should also highly recommend, that you should place some ads onto your page.
You can find the button for the [b][i]Ad Catalogue[/i][/b] in the top left of your screen. Then click on an ad which has the certain [i]je ne sais quoi[/i].
Then click and drag it onto the template as well.

Once you are done, confirm with the button in the bottom right.

Have fun in your [wave][rainbow][i]limited space[/i][/rainbow][/wave].
[color=#577278aa]Would be a fun idea for a special edition.[/color]
	""",
	ID.SCENE_TYPES.RATING: """
Alright, now that you have designed your newspaper page, we are able through our advanced rabbit technology to instantly evaluate how well the page will [i]land[/i] with the other rabbits.
In other words, we'll see if it is truly [i]streets ahead[/i].

The basic rating is based on the headlines you chose - based on the tone and how informative they are. Then we calculate the average.

On top of that, there are various bonuses and penalties that you can [i]earn[/i]. But I'll let you discover those by yourself.
[color=#577278aa]Well, one bonus and two penalties. The tech department tells me that they ran out of time to come up with more...[/color]

Based on the final rating, you'll gain or lose prestige in the local rabbit community.
The more prestige you have, the better.

After that, a new day begins and you can do it all over!

Every day, the other rabbits have new stories to tell.
So go forth and do more interviews [i]in the streets[/i].
	"""
}


var stories = {
	ID.STORIES.TEST_1: StoryData.new(
		'TEST_1',
		'test_1',
		17,
		[
			'Test 1 Fact 1',
			'',
			'',
		],
		[
			[
				[ 'Test 1 - happy', 5 ],
				[ 'Test 1 - neutral', 4 ],
				[ 'Test 1 - sad', 'test_1_sad' ],
			],
			[],
			[]
		]
	),
	ID.STORIES.TEST_2: StoryData.new(
		'TEST_2',
		'test_2',
		17,
		[
			'Test 2 Fact 1',
			'Test 2 Fact 2',
			'',
		],
		[
			[],
			[
				[ 'Test 2 - happy', 5 ],
				[ 'Test 2 - neutral', 4 ],
				[ 'Test 2 - very very sad', 1 ],
			],
			[]
		]
	),
	ID.STORIES.TEST_3: StoryData.new(
		'TEST_3',
		'test_3',
		17,
		[
			'Test 3 Fact 1',
			'Test 3 Fact 2',
			'Test 3 Fact 3',
		],
		[
			[],
			[],
			[
				[ 'Test 3 - happy', 5 ],
				[ 'Test 3 - neutral', 4 ],
				[ 'Test 3 - sad', 3 ],
			]
		]
	),
	ID.STORIES.ONE_KG_CARROT: StoryData.new(
		'Grandma grows 1kg carrot',
		'one_kg_carrot',
		17,
		[
			'Grandma has been growing loads of "thick" carrots',
			'The biggest one is 1kg big, whoa',
			'They are traffic cone - orange',
		],
		[
			[
				[ 'Carrots!', 3 ],
			],
			[
				[ '1kg big carrot', 4 ],
			],
			[
				[ 'Grandma breaks all records with 1kg traffic cone carrot', 5 ],
			]
		]
	),
	ID.STORIES.VEGETARIAN_OPTIONS: StoryData.new(
		'Canteen has new vegetarian options',
		'vegetarian_options',
		17,
		[
			"Didn't have vegetarian options before...weird",
			'There are a whole 5 new dishes',
			'Are experiencing a huge burst of attention',
		],
		[
			[
				[ 'Vegetarian options available - now!', 4]
			],
			[
				[ '5 new meals available in canteen - all vegetarian', 5]
			],
			[
				[ 'The hottest meals in town - all vegetarian', 3]
			]
		]
	),
	ID.STORIES.BOOKCLUB_NEW_BOOK: StoryData.new(
		'The bookclub of the university reads a new book',
		'bookclub_new_book',
		17,
		[
			"It's very sad and the ending is unbelievable",
			'The name is "Romeo and Juliet", an all-time classic',
			'In the end they both die',
		],
		[
			[
				[ "Local book club reads new book - it's very sad", 5]
			],
			[
				[ 'University book club reads classic "Romeo and Juliet"', 4]
			],
			[
				[ """Everyone dies at the end in book club's newest book "Romeo and Juliet" """, 1]
			]
		]
	),
	ID.STORIES.MENTHOL_IN_EYES: StoryData.new(
		"A kid got something into it's eyes",
		'menthol_in_eyes',
		17,
		[
			"It's menthol that's in the eyes",
			'The kid thought it was eye drops',
			'Needed to go to the doctors, because the eyes were completely red',
		],
		[
			[
				[ "Kid got menthol in eyes and cried", 5 ],
				[ "Kids hate this - Menthol", 4 ],
			],
			[
				[ "Kid got tricked by bottle manufacturer - Menthol instead of eye drops", 4 ],
				[ "Kid mistook Menthol for eye drops", 2 ]
			],
			[
				[ "5 hours at the doctors - only because of a small mistake", 4],
				[ "Don't mistake Menthol for eye drops, kids!", 5 ]
			]
		]
	),
	ID.STORIES.CARROT_FLUTE: StoryData.new(
		'I heard someone play the flute',
		'carrot_flute',
		17,
		[
			"The flute was a Rabivari from Italy and it's made out of a carrot",
			"There is a complete orchestra for it",
			"They will give a concert soon" 
		],
		[
			[
				[ "Carrot flutes exist", 4],
				[ "10 Tips to use your carrot", 5]
			],
			[
				[ "The orchestra YOU didn't know of", 4],
				[ "Local orchestra uses rare instrument", 5],
				[ "Orchestra presents new instrument", 3]
			],
			[
				[ "Orchestra with rare instrument gives concert", 4],
				[ "Rare instrument can be seen live soon", 3],
				[ "Local concert by the rabbit orchestra soon", 2]
			]
		]
	),
	ID.STORIES.KIDS_PARTY: StoryData.new(
		'A real vampire?',
		'kids_party',
		17,
		[
			"The vampire was hired for a kids party",
			"He frightened the children",
			"Also he got a lot of money for it",
		],
		[
			[
				[ "Vampire - new party tip", 3],
				[ "Kids party hires real vampire", 4]
			],
			[
				[ "Perfect for Halloween - Hire your local vampire", 4]
			],
			[
				[ "Are you afraid of vampires? Maybe, once you see this bill", 5]
			]
		]
	),
	ID.STORIES.RABBIT_DERULO: StoryData.new(
		'Rabbit Derulo is performing!',
		'rabbit_derulo',
		17,
		[
			 """Apparently he's the "bestest singer on the planet!" """,
			'He is performing at Burrow Stadium',
			'He is very popular'
		],
		[
			[
				[ "The amazing Rabbit Derulo performance", 4]
			],
			[
				[ 'Burrow Stadium welcomes Rabbit Derulo', 4]
			],
			[
				[ "Rabbit Derulo tickets sold out for the night", 5]
			]
		]
	),
	ID.STORIES.LOST_BALL: StoryData.new(
		"A kid with cheese on it's shirt is crying",
		'lost_ball',
		17,
		[
			"It lost it's ball, that's why it's crying",
			'The ball was in the dining area',
			"Kid learned a valuable life lesson...to always depend on adults, great",
		],
		[
			[
				[ "Have you seen this ball?", 4],
				[ "5 Tips to never loose your ball", 4]
			],
			[
				[ "Don't steal the ball in the dining area", 1],
				[ "Lost ball found in dining area", 3]
			],
			[
				[ "Kid and ball finally reunited", 5],
				[ "Lost ball back with kid - Kid happy again", 4],
				[ "Ball found - lost kid", 3]
			]
		]
	),
	ID.STORIES.NEW_FERTILIZER: StoryData.new(
		"The woman in the carrot dress found a new fertilizer",
		'new_fertilizer',
		17,
		[
			'The fertilizer is called "Water+" and consists of a whopping 130% water',
			"Everyone uses it now thanks to her tip",
			"It also works very good for vegetables (esp. carrots)",
		],
		[
			[
				[ "Water+ - the new fertilizer", 1],
				[ "Flowers grow perfect now", 3],
			],
			[
				[ "Local group creates beautiful garden", 4],
				[ "One product changed everything for them", 5],
			],
			[
				[ "Wonder fertilizer surprises everyone", 4],
				[ '"I was stupid to use something else"', 3],
			]
		]
	),
	ID.STORIES.PET_BEAR: StoryData.new(
		'Bears are friends',
		'pet_bear',
		17,
		[
			'Rabbit escaped bear with his life',
			'He tried to pet the bear unsuccessfully...',
		],
		[
			[
				[ "Survived and pet the bear! Local hero exclaims", 5]
			],
			[
				[ 'Rabbit chased away from bear pets', 4]
			],
			[
				[ "Idiot pets bear", 1]
			]
		]
	),
	ID.STORIES.DUAL_WIELD: StoryData.new(
		'Double the tool',
		'dual_wield',
		17,
		[
			'Rabbit thinks he looks cool with two rakes... he does not...',
		],
		[
			[
				[ "Two rakes for the price of one rabbit", 1]
			],
			[
			],
			[
			]
		]
	),
	
	ID.STORIES.CARROT_FRUIT: StoryData.new(
		'Vegetable trouble...',
		'carrot_fruit',
		17,
		[
			'Rabbit thinks carrots are... fruits?',
		],
		[
			[
				[ "Superfood carrots in all categories!", 5]
			],
			[
				[ 'Fruit vs Vegetable - you decide!', 4]
			],
			[
				[ "Carrot studies are inconclusive - still veggies", 3]
			]
		]
	),
	ID.STORIES.JPEG: StoryData.new(
		'Ludum Dare assets',
		'jpeg',
		17,
		[
			'Ludum Dare is happening and this rabbit is working on assets',
			'They hand painted the carrot!',
			'But then they just renamed the file ending instead of converting the image.',
		],
		[
			[
				[ "Ludum Dare!", 5]
			],
			[
				[ 'Hand painted assets are the new rage!', 5],
				[ 'Are hand painted assets worth it?', 3],
			],
			[
				[ 'Ludum Dare 54 - a fun event', 5],
				[ 'Inside scoop: Working on jam games can be stressful!', 5],
				[ 'Rabbit scanners on sale', 1],
			]
		]
	),
	
	ID.STORIES.THIEF: StoryData.new(
		'One munch thief',
		'thief',
		17,
		[
			'Thief on the loose since a week',
			'Takes one whole bite out of every crop',
			'Anti is going to use paint to catch thief'
		],
		[
			[
				[ "One munch thief strikes again!", 4]
			],
			[
				[ 'Some vegetables stolen amongst rabbits', 5]
			],
			[
				[ "Poor rabbit nibbles vegetables, says selfish neighbour", 3]
			]
		]
	),
}


var ads = {
	ID.ADS.WATER: AdData.new(
		preload('res://assets/sprites/water_advert.png'),
		[
			ID.STORIES.NEW_FERTILIZER,
		]
	),
	ID.ADS.CARROTS: AdData.new(
		preload('res://assets/sprites/carrot_advert.png'),
		[
			ID.STORIES.ONE_KG_CARROT,
			ID.STORIES.JPEG,
			ID.STORIES.CARROT_FRUIT,
		]
	),
	ID.ADS.RAKES: AdData.new(
		preload('res://assets/sprites/rake_advert.png'),
		[
			ID.STORIES.DUAL_WIELD,
		]
	),
	ID.ADS.BEAR: AdData.new(
		preload('res://assets/sprites/reality_claw_advert.png'),
		[
			ID.STORIES.KIDS_PARTY,
			ID.STORIES.PET_BEAR,
		]
	),
	ID.ADS.KNIFE: AdData.new(
	preload('res://assets/sprites/knife_advert.png'),
		[
		]
	),
}

var timelines = {
	ID.CHARACTERS.GIRL_RABBIT: [ID.TIMELINES.VEGETARIAN_OPTIONS],
	ID.CHARACTERS.SAD_RABBIT: [ID.TIMELINES.BOOKCLUB_NEW_BOOK],
	ID.CHARACTERS.CHRISTMAS_CHILD_RABBIT: [ID.TIMELINES.MENTHOL_IN_EYES],
	ID.CHARACTERS.BIKER_CHILD_RABBIT: [ID.TIMELINES.RABBIT_DERULO],
	ID.CHARACTERS.GRANDMA: [ID.TIMELINES.ONE_KG_CARROT],
	ID.CHARACTERS.EDGY_RABBIT: [ID.TIMELINES.CARROT_FLUTE],
	ID.CHARACTERS.VAMPIRE_RABBIT: [ID.TIMELINES.KIDS_PARTY],
	ID.CHARACTERS.PLAID_RABBIT: [ID.TIMELINES.PET_BEAR],
	ID.CHARACTERS.PINK_CHILD_RABBIT: [ID.TIMELINES.ONE_KG_CARROT],
	ID.CHARACTERS.PAJAMA_RABBIT: [ID.TIMELINES.DUAL_WIELD],
	ID.CHARACTERS.CHEESE_CHILD_RABBIT: [ID.TIMELINES.LOST_BALL],
	ID.CHARACTERS.GREEN_RABBIT: [ID.TIMELINES.CARROT_FRUIT],
	ID.CHARACTERS.CARROT_DRESS_RABBIT: [ID.TIMELINES.NEW_FERTILIZER],
	ID.CHARACTERS.LD_RABBIT: [ID.TIMELINES.JPEG],
	ID.CHARACTERS.ANTI_RABBIT: [ID.TIMELINES.THIEF],
	ID.CHARACTERS.BALL: [ID.TIMELINES.BALL],
	
}
