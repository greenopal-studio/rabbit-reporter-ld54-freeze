class_name AdData


var _texture: Resource
var _story_synergy_ids: Array[ID.STORIES]


func _init(
    __texture: Resource,
    __story_synergy_ids: Array[ID.STORIES],
):
    _texture = __texture
    _story_synergy_ids = __story_synergy_ids


func get_texture() -> Resource:
    return _texture


func get_story_synergy_ids() -> Array[ID.STORIES]:
    return _story_synergy_ids
