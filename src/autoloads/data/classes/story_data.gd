class_name StoryData


var _name: String
var _dialogic_var_name: String
var _lifetime: int
var _info_bullet_points: Array[String]
var _headlines: Array[Array]


func _init(
	__name: String,
	__dialogic_var_name: String,
	__lifetime: int,
	__info_bullet_points: Array[String],
	__headlines: Array[Array],
):
	_name = __name
	_dialogic_var_name = __dialogic_var_name
	_lifetime = __lifetime
	_info_bullet_points = __info_bullet_points
	_headlines = __headlines
	

func get_name() -> String:
	return _name


func get_dialogic_var_name() -> String:
	return _dialogic_var_name


func get_lifetime() -> int:
	return _lifetime


func get_info_bullet_points() -> Array[String]:
	return _info_bullet_points


func get_info_bullet_point_at_index(__idx: int) -> String:
	if __idx >= 0 and __idx < len(_info_bullet_points):
		return _info_bullet_points[__idx]
	return ''


func get_headlines() -> Array[Array]:
	return _headlines
