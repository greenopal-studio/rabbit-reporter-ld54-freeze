class_name State extends Node


# INTERNAL STATE DATA:

var rng := RandomNumberGenerator.new()
var _logger: Logger = LOGGING.get_logger('STATE')

var _seen_hints: Array[ID.SCENE_TYPES]
const _any_location_scenes = [
	ID.SCENE_TYPES.RABBIT_UNIVERSITY,
	ID.SCENE_TYPES.VEGETABLE_PATCH,
	ID.SCENE_TYPES.DINING_AREA,
]

var _day: int
var _prestige: float
var _available_stories: Dictionary
var _collected_stories: Array[CollectedStoryState]
var _picked_ads: Array[ID.ADS]
var _placed_stories: Array[PlacedStoryState]


static func get_type_hints() -> Dictionary:
	return {
	}


func reset() -> void:
	rng.randomize()

	_seen_hints = []

	_day = 1
	_prestige = 10.0
	_collected_stories = []
	_picked_ads = []
	_placed_stories = []

	__current_save_id = ''
	
	_distribute_stories()


func _distribute_stories() -> void:
	for __character in ID.CHARACTERS.values():
		var __previous_story = null
		if _available_stories.has(__character):
			__previous_story = _available_stories[__character]
		
		var __character_timelines = DATA.timelines[__character]
		if __previous_story == null or __character_timelines.size() == 1:
			_available_stories[__character] = __character_timelines[rng.randi_range(0, __character_timelines.size() - 1)]
		else:
			var __cleaned_timelines = []
			for __timeline in __character_timelines:
				if __timeline != __previous_story:
					__cleaned_timelines.append(__timeline)
			_available_stories[__character] = __cleaned_timelines[rng.randi_range(0, __cleaned_timelines.size())]


# STATE DATA GETTERS:

func get_has_seen_hint(__scene_id: ID.SCENE_TYPES) -> bool:
	if __scene_id in _any_location_scenes:
		return ID.SCENE_TYPES._ANY_LOCATION in _seen_hints
	else:
		return __scene_id in _seen_hints


# yes this is not technically state related, but shrug, time crunch, innit
func get_hint(__scene_id: ID.SCENE_TYPES) -> String:
	if __scene_id in _any_location_scenes:
		return DATA.hints[ID.SCENE_TYPES._ANY_LOCATION]
	else:
		return DATA.hints[__scene_id] if __scene_id in DATA.hints else ''


func get_day() -> int:
	return _day


func get_prestige() -> float:
	return _prestige


func get_available_story(__character_id: ID.CHARACTERS) -> String:
	return _available_stories[__character_id]


func get_collected_stories() -> Array[CollectedStoryState]:
	return _collected_stories


func get_collected_story(__story_id: ID.STORIES) -> CollectedStoryState:
	for story in _collected_stories:
		if story.get_story_id() == __story_id:
			return story
	return null


func is_story_collected(__story_id: ID.STORIES) -> bool:
	for story in _collected_stories:
		if story.get_story_id() == __story_id:
			return true
	return false


func get_picked_ads() -> Array[ID.ADS]:
	return _picked_ads


func get_placed_stories() -> Array[PlacedStoryState]:
	return _placed_stories


# SIGNALS AND HANDLERS:

signal seen_hint(__scene_id: ID.SCENE_TYPES)

func _on_seen_hint(__scene_id: ID.SCENE_TYPES) -> void:
	if __scene_id in _any_location_scenes:
		_seen_hints.append(ID.SCENE_TYPES._ANY_LOCATION)
	else:
		_seen_hints.append(__scene_id)

signal new_day()

func _on_new_day() -> void:
	_day += 1

	_collected_stories = []
	_picked_ads = []
	_placed_stories = []

	_distribute_stories()


func _on_dialogic_variable_changed(__info):	
	if not __info['variable'].begins_with(DATA.DIALOGIC_STORIES_VAR_PREFIX):
		return
		
	var var_name = __info['variable'].replace(DATA.DIALOGIC_STORIES_VAR_PREFIX, '')
	var new_value = __info['new_value']

	for story_id in DATA.stories.keys():
		var story_data = DATA.stories[story_id]
		if story_data.get_dialogic_var_name() == var_name:
			for story_state in _collected_stories:
				if story_state.get_story_id() == story_id:
					collected_story_info_changed.emit(story_id, new_value)
					return
			_collected_stories.append(
				CollectedStoryState.new(
					story_id,
					_day
				)
			)
			collected_stories_changed.emit(_collected_stories)
			collected_story_info_changed.emit(story_id, new_value)
			return

signal collected_story_info_changed(__story_id: ID.STORIES, __new_count: int)

signal collected_stories_changed(__new_stories: Array[CollectedStoryState])

signal pick_story(__story_id: ID.STORIES)

func _on_pick_story(__story_id: ID.STORIES) -> void:
	for story in _collected_stories:
		if story.get_story_id() != __story_id:
			story.set_is_used(true)

	collected_stories_changed.emit(_collected_stories)

signal pick_ad(__ad_id: ID.ADS)

func _on_pick_ad(__ad_id: ID.ADS) -> void:
	_picked_ads.append(__ad_id)

signal place_story(
	__story_id: ID.STORIES,
	__headline: String, __rating: int, __pso: Vector2, __size: Vector2,
	__is_headline: bool
)

func _on_place_story(
	__story_id: ID.STORIES,
	__headline: String, __rating: int, __pos: Vector2, __size: Vector2,
	__is_headline: bool
) -> void:
	var update = false
	for story in _placed_stories:
		if story.get_used_story_id() == __story_id:
			story.set_headline(__headline)
			story.set_rating(__rating)
			story.set_pos(__pos)
			story.set_size(__size)
			update = true
	if not update:
		_placed_stories.append(PlacedStoryState.new(
			__story_id, -1 as ID.ADS, __headline, __rating, __pos, __size, __is_headline
		))

	placed_stories_changed.emit(_placed_stories)

signal place_ad(__ad_id: ID.ADS, __pos: Vector2)

func _on_place_ad(__ad_id: ID.ADS, __pos: Vector2) -> void:
	var update = false
	for ad in _placed_stories:
		if ad.get_used_ad_id() == __ad_id:
			ad.set_pos(__pos)
			update = true
	if not update:
		_placed_stories.append(PlacedStoryState.new(
			-1, __ad_id, '', -1 as ID.STORIES, __pos, Vector2(1, 1), false
		))

signal remove_placed_story(__story_id: ID.STORIES)

func _on_remove_placed_story(__story_id: ID.STORIES) -> void:
	var updated: Array[PlacedStoryState] = []
	for story in _placed_stories:
		if story.get_used_story_id() != __story_id:
			updated.append(story)

	_placed_stories = updated
	placed_stories_changed.emit(_placed_stories)

signal remove_placed_ad(__ad_id: ID.ADS)

func _on_remove_placed_ad(__ad_id: ID.ADS) -> void:
	var updated: Array[PlacedStoryState] = []
	for ad in _placed_stories:
		if ad.get_used_ad_id() != __ad_id:
			updated.append(ad)

	_placed_stories = updated
	placed_stories_changed.emit(_placed_stories)

signal placed_stories_changed(__new_stories: Array[PlacedStoryState])

signal update_prestige(__diff: float)

func _on_update_prestige(__diff: float) -> void:
	_prestige += __diff
	prestige_changed.emit(_prestige)

signal prestige_changed(__new_prestige: float)


# SAVESTATE METHODS:

const _SUB_PATH = 'savestate'
const _CURRENT_VERSION = 0

var __current_save_id: String = ''


func get_current_save_id() -> String:
	return __current_save_id


func get_savestate_files() -> PackedStringArray:
	if DirAccess.dir_exists_absolute('user://' + _SUB_PATH):
		return DirAccess.get_files_at('user://' + _SUB_PATH)
	return []


func save_data(__meta: Dictionary = {}) -> void:
	if not __current_save_id:
		__current_save_id = SerializeService.save_data_with_generated_id(
			_SUB_PATH, _CURRENT_VERSION, __meta, self
		)
	else:
		SerializeService.save_data_with_id(
			_SUB_PATH, __current_save_id, _CURRENT_VERSION, __meta, self
		)


func load_meta(__save_id: String) -> Variant:
	return SerializeService.load_meta_with_id(_SUB_PATH, __save_id)


func load_data(__save_id: String) -> bool:
	var success = SerializeService.load_data_with_id_into_object(_SUB_PATH, __save_id, State, self)
	if success:
		__current_save_id = __save_id	
	return success


# HELPER METHODS:

# vararg is not supported in GDscript (yet) :/

func _log_signal_0(__signal_name: String) -> void:
	_logger.info('got signal %s'%[
		__signal_name
	])

func _log_signal_1(__arg1, __signal_name: String) -> void:
	_logger.info('got signal %s with argument %s'%[
		__signal_name, __arg1
	])

func _log_signal_2(__arg1, __arg2, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s'%[
		__signal_name, __arg1, __arg2
	])

func _log_signal_3(__arg1, __arg2, __arg3, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s , %s'%[
		__signal_name, __arg1, __arg2, __arg3
	])

func _log_signal_4(__arg1, __arg2, __arg3, __arg4, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s , %s , %s'%[
		__signal_name, __arg1, __arg2, __arg3, __arg4
	])

func _log_signal_5(__arg1, __arg2, __arg3, __arg4, __arg5, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s , %s , %s , %s'%[
		__signal_name, __arg1, __arg2, __arg3, __arg4, __arg5
	])

func _log_signal_6(__arg1, __arg2, __arg3, __arg4, __arg5, __arg6, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s , %s , %s , %s , %s'%[
		__signal_name, __arg1, __arg2, __arg3, __arg4, __arg5, __arg6
	])


func _ready():
	reset()

	# Automatically connect _on_ handler methods to signals:
	for method_dict in get_method_list():
		var method_name = method_dict['name']
		if not method_name.begins_with('_on_'):
			continue

		if method_name.begins_with('_on_dialogic_'):
			continue

		var signal_name = method_name.substr(4) # len('_on_') = 4
		if !has_signal(signal_name):
			_logger.error('No matching signal defined for handler method %s! You should add a %s signal.'%[
				method_name, signal_name
			])
			continue

		connect(signal_name, Callable(self, method_name))

	for signal_dict in get_signal_list():
		match len(signal_dict.args):
			0:
				connect(signal_dict['name'], Callable(self, '_log_signal_0').bind(signal_dict['name']))
			1:
				connect(signal_dict['name'], Callable(self, '_log_signal_1').bind(signal_dict['name']))
			2:
				connect(signal_dict['name'], Callable(self, '_log_signal_2').bind(signal_dict['name']))
			3:
				connect(signal_dict['name'], Callable(self, '_log_signal_3').bind(signal_dict['name']))
			4:
				connect(signal_dict['name'], Callable(self, '_log_signal_4').bind(signal_dict['name']))
			5:
				connect(signal_dict['name'], Callable(self, '_log_signal_5').bind(signal_dict['name']))
			6:
				connect(signal_dict['name'], Callable(self, '_log_signal_6').bind(signal_dict['name']))
			_:
				_logger.warning('Too many args for signal %s, please add an appropriate _log_signal_%d func.'%[
					signal_dict['name'], len(signal_dict.args)
				])
				connect(signal_dict['name'], Callable(self, '_log_signal_0').bind(signal_dict['name']))
 
	# integrate Dialogic:

	await get_tree().process_frame # we need to wait for dialogic to be fully initialized
	
	Dialogic.VAR.variable_changed.connect(_on_dialogic_variable_changed)
