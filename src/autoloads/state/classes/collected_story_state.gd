class_name CollectedStoryState extends SerializableObject


var __logger = LOGGING.get_logger('CollectedStoryState')


var _story_id: ID.STORIES
var _collected_on_day: int
var _is_used: bool


func _init(
	__story_id: ID.STORIES = 0 as ID.STORIES,
	__collected_on_day: int = 0,
	__is_used: bool = false,
):
	_story_id = __story_id
	_collected_on_day = __collected_on_day
	_is_used = __is_used


func get_story_id() -> ID.STORIES:
	return _story_id


func get_collected_on_day() -> int:
	return _collected_on_day


func is_fresh() -> bool:
	return DATA.stories[_story_id].get_lifetime() >= (STATE.get_day() - _collected_on_day)


func is_used() -> bool:
	return _is_used


func set_is_used(__is_used: bool) -> void:
	_is_used = __is_used


func get_info_count() -> int:
	var var_value: Variant = Dialogic.VAR.get_variable(
		DATA.DIALOGIC_STORIES_VAR_PREFIX + DATA.stories[_story_id].get_dialogic_var_name(),
		0
	)
	match typeof(var_value):
		TYPE_STRING:
			return int(var_value)
		TYPE_FLOAT:
			return int(var_value)
		TYPE_INT:
			return var_value
		_:
			__logger.error('Got unexpected value type from dialogic var')
			return 0


func get_headlines() -> Array[Array]:
	var result: Array[Array] = []

	for headline_level in DATA.stories[_story_id].get_headlines():
		var level_result = [] 

		for headline_data in headline_level:
			var headline_text = headline_data[0]
			var headline_approval = headline_data[1]
			match typeof(headline_approval):
				TYPE_INT:
					pass # ok
				TYPE_STRING:
					var var_value: Variant = Dialogic.VAR.get_variable(
						DATA.DIALOGIC_HEADLINE_APPROVAL_VAR_PREFIX + headline_approval,
						1
					)
					match typeof(var_value):
						TYPE_STRING:
							headline_approval = int(var_value)
						TYPE_FLOAT:
							headline_approval = int(var_value)
						TYPE_INT:
							headline_approval = var_value
						_:
							headline_approval = 1
							__logger.error('Got unexpected value type from dialogic var')
				_:
					headline_approval = 1
					__logger.error('Got unexpected value type for headline approval value')

			if headline_approval < 1:
				headline_approval = 1

			level_result.append([
				headline_text, headline_approval
			])

		result.append(level_result)
		
	return result
