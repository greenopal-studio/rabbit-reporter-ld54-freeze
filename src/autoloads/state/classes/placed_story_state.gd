class_name PlacedStoryState extends SerializableObject

var _used_story_id: ID.STORIES
var _used_ad_id: ID.ADS
var _headline: String
var _rating: int
var _pos: Vector2
var _size: Vector2
var _is_headline: bool


func _init(
	__used_story_id: ID.STORIES,
	__used_ad_id: ID.ADS,
	__headline: String,
	__rating: int,
	__pos: Vector2,
	__size: Vector2,
	__is_headline: bool,
):
	_used_story_id = __used_story_id
	_used_ad_id = __used_ad_id
	_headline = __headline
	_rating = __rating
	_pos = __pos
	_size = __size
	_is_headline = __is_headline


func get_used_story_id() -> ID.STORIES:
	return _used_story_id


func get_used_ad_id() -> ID.ADS:
	return _used_ad_id


func get_headline() -> String:
	return _headline


func set_headline(__headline: String) -> void:
	_headline = __headline


func get_rating() -> int:
	return _rating


func set_rating(__rating: int) -> void:
	_rating = __rating


func get_pos() -> Vector2:
	return _pos


func set_pos(__pos: Vector2) -> void:
	_pos = __pos


func get_size() -> Vector2:
	return _size


func set_size(__size: Vector2) -> void:
	_size = __size


func get_is_headline() -> bool:
	return _is_headline
