extends Node

var console_window_scene = preload('res://scenes/dev_console/dev_console_window.tscn')


var console_window


func _ready():
	console_window = console_window_scene.instantiate()
	add_child(console_window)


func _input(__event):
	if UtilService.is_debug() and __event.is_action_pressed('debug_dev_console'):
		console_window.popup()
