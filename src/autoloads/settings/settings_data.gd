class_name SettingsData extends SerializableObject

var _volume: float
var _font_scale: float


func _init(
	__volume: float = 0.0,
	__font_scale: float = 1.0
):
	_volume = __volume
	_font_scale = __font_scale


func duplicate() -> SettingsData:
	return SettingsData.new(
		_volume,
		_font_scale
	)


func set_volume(__new_volume: float) -> bool:
	if _volume == __new_volume:
		return false
	_volume = __new_volume
	return true


func get_volume() -> float:
	return _volume
	

func set_font_scale(__new_value: float) -> bool:
	if _font_scale == __new_value:
		return false
	_font_scale = __new_value
	return true


func get_font_scale() -> float:
	return _font_scale
