extends Node

var _current_loggers: Dictionary = {}


func get_logger(__name: String) -> Logger:
	if __name in _current_loggers:
		return _current_loggers[__name]

	var logger = Logger.new(__name)
	_current_loggers[__name] = logger
	return logger
