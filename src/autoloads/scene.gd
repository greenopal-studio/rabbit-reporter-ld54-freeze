extends Node


"""
We use this instead of SceneTree.change_scene_to_file, because that method would also replace our singletons.
"""

var scene_paths = {
	ID.SCENE_TYPES.TEST: 'res://scenes/test/test.tscn',
	ID.SCENE_TYPES.MENU: 'res://scenes/menu/menu.tscn',
	ID.SCENE_TYPES.SETTING: 'res://scenes/settings/settings.tscn',
	ID.SCENE_TYPES.WORLD_MAP: 'res://scenes/world/map/map.tscn',
	ID.SCENE_TYPES.ASSEMBLE_NEWSPAPER: 'res://scenes/assemble/assemble.tscn',
	ID.SCENE_TYPES.RABBIT_UNIVERSITY: 'res://scenes/world/levels/rabbit_university.tscn',
	ID.SCENE_TYPES.VEGETABLE_PATCH: 'res://scenes/world/levels/vegetable_patch.tscn',
	ID.SCENE_TYPES.DINING_AREA: 'res://scenes/world/levels/dining_area.tscn',
	ID.SCENE_TYPES.RATING: 'res://scenes/rating/rating.tscn',
}

var _logger: Logger = LOGGING.get_logger('SCENE')

var _current_scene = null
var _current_scene_id: ID.SCENE_TYPES = -1 as ID.SCENE_TYPES


func _ready():
	var root = get_tree().get_root()
	_current_scene = root.get_child(root.get_child_count() - 1)


func goto_scene(__scene_id: ID.SCENE_TYPES):
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# So we have to give it some time.
	call_deferred("__deferred_goto_scene", __scene_id)


func __deferred_goto_scene(__scene_id: ID.SCENE_TYPES):
	_current_scene.free()

	_current_scene_id = __scene_id

	var new_scene: PackedScene = ResourceLoader.load(scene_paths[__scene_id])
	_current_scene = new_scene.instantiate()

	get_tree().get_root().add_child(_current_scene)
	get_tree().set_current_scene(_current_scene)

	_logger.info('switched to scene #%s loaded from %s'%[ __scene_id, scene_paths[__scene_id] ])


func get_current_scene_id() -> ID.SCENE_TYPES:
	return _current_scene_id
