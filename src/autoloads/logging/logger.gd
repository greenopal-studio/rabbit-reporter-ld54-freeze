class_name Logger


var fail_dialog_scene = preload('res://scenes/fail_dialog/fail_dialog.tscn')

var _name: String


func _init(
	__name: String,
):
	_name = __name


## To be used for failures, i.e. a state which prevents the code flow from continuing.
## Calling this method also halts the editor in debug mode (via an assert).
## Calling this method also displays a dialog to inform the player about the failure.
func fail(__user_message: String, __log_message: String) -> void:
	push_error(_format_message('FAIL', __log_message))
	
	var dialog = fail_dialog_scene.instantiate()
	dialog.set_text(__user_message)
	LOGGING.get_tree().get_root().add_child(dialog)
	
	@warning_ignore("assert_always_false") # we deliberately want to halt the editor
	assert(false, __log_message) # keep as last line as it aborts the func


## To be used for recoverable errors, i.e. a state which does not prevent the code flow from
##  continuing but which makes special handling necessary.
## Calling this method also halts the editor in debug mode (via an assert).
func error(__message: String) -> void:
	push_error(_format_message('ERROR', __message))
	
	@warning_ignore("assert_always_false") # same here
	assert(false, __message)


## To be used for warning, i.e. a state which might produce unintended side effects. 
func warning(__message: String) -> void:
	push_warning(_format_message('WARN', __message))


## To be used for informations about actions, i.e. to add context to eventual errors / warning.
func info(__message: String) -> void:
	print(_format_message('INFO', __message))


## To be used for debugging, i.e. to add extra information while running in debug mode.
func debug(__message: String) -> void:
	print_debug(_format_message('DEBUG', __message))


func _format_message(__level: String, __message: String) -> String:
	@warning_ignore('INTEGER_DIVISION') # get_time_string expects an int without decimal part
	return '[%s][%s][%s] %s'%[
		Time.get_time_string_from_unix_time(Time.get_ticks_msec() / 1_000),
		__level, _name, __message
	]
